# Introduction
You can contribute to the project through any of the following actions:

1. Help with the [development](#Development)
2. Bug reports, enhancement suggestions, and any other kind of [feedback](#Feedback).
3. Help with the [translation](#Translations)
4. Make a [donation](#Donations)
5. [Share your experience](#Community) with others

Also, we suggest you to follow our [*code of conduct*](code_of_conduct.md) at every interaction with the project.


# Development
- **Design choices**
  - Platforms: *Linux (Flatpak)*, *Windows*, *MacOS*, *BSD*, *Web*
  - Languages: *Elm*
  - Design pattern: *Elm architecture*
  - Coding convention: *none*
  - Versioning system: [*Semantic Versioning 2.0.0*](https://semver.org/spec/v2.0.0.html)
  - Pull requests protocol: Follow the [*project template*](./pull_request_template.md)
  - Unit testing style: *WIP*
  - UI convention: *WIP*
- **Tooling choices**
  - Compilers/interpreters: *elm-make*
  - REPL tool: *elm-repl*
  - VCS tool: *git*
  - Package manager: *elm-get*
  - Build automation tool: *none* 
  - Automatic deployment manager: **none* 
  - LSP: *elm-language-server*
  - Code formater: *elm-format*
  - Containerized coding environment: *"Development Containers" config files*
- **Libraries, frameworks and other dependencies**
  - *.editorconfig*


# Feedback
You can report bugs, request features, and suggest enhancements by fulfilling a formal template stored on the *docs/* folder as *[issue_template.md](./issue_template.md)*.


# Translations
Official translations: WIP


# Donations
Supported donation platforms: WIP


# Community
Supported communication channels: WIP
